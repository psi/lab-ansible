---

- name: Install ROOT
  gather_facts: true
  hosts: all
  vars_files:
  - vars.yml

  tasks:

  - name: Install EPEL release
    become: true
    ansible.builtin.dnf:
      name:
      - epel-release

  - name: Enable code ready builds
    become: true
    ansible.builtin.command:
      sudo dnf config-manager --set-enabled crb

  - name: Install required dependencies for ROOT
    become: true
    ansible.builtin.dnf:
      name:
      - avahi-compat-libdns_sd-devel  # requires CRB
      - binutils
      - cfitsio-devel  # requires EPEL
      - cmake
      - fftw-devel
      - ftgl-devel  # requires EPEL
      - gcc
      - gcc-c++
      - git
      - git-lfs
      - glew-devel  # requires EPEL
      - gnu-free-fonts-common
      - gnu-free-mono-fonts
      - gnu-free-sans-fonts
      - gnu-free-serif-fonts
      - graphviz-devel  # requires CRB
      - gsl-devel
      - json-devel
      - libAfterImage
      - libarrow
      - libuuid-devel
      - libusb-devel
      - libX11-devel
      - libXext-devel
      - libXft-devel
      - libXpm-devel
      - make
      - openldap-devel
      - openssl-devel
      - python3-devel
      - python3-numpy
      - qt5-qtwebengine-devel  # requires EPEL
      - re2
      - readline-devel
      - xorg-x11-fonts-ISO8859-1-75dpi
      - xorg-x11-xauth
      - xrootd
      state: present

  - name: Check if ROOT directory exists already
    ansible.builtin.stat:
      path: '{{ ansible_user_dir }}/root_{{ root_version }}'
    register: root_installation

  - name: Download and compile ROOT
    block:
      - name: Download ROOT source
        ansible.builtin.unarchive:
          src: 'https://root.cern/download/root_v{{ root_version }}.source.tar.gz'
          dest: '{{ ansible_user_dir }}'
          creates: '{{ ansible_user_dir }}/root-{{ root_version }}'
          remote_src: yes

      - name: Configure and build ROOT
        ansible.builtin.shell: |
          mkdir -p {{ ansible_user_dir }}/root_{{ root_version }}_build
          cd {{ ansible_user_dir }}/root_{{ root_version }}_build
          cmake -DCMAKE_INSTALL_PREFIX={{ ansible_user_dir }}/root_{{ root_version }} -DPython3_EXECUTABLE=python{{ python_version}} -Droot7=OFF {{ ansible_user_dir }}/root-{{ root_version }}
          cmake --build . --target install -j{{ ansible_processor_cores }}

      - name: Clean up ROOT build
        ansible.builtin.file:
          path: "{{ item }}"
          state: absent
        loop:
          - "{{ ansible_user_dir }}/root-{{ root_version }}"
          - "{{ ansible_user_dir }}/root_{{ root_version }}_build"

      - name: Add ROOT setup to .bashrc
        ansible.builtin.lineinfile:
          path: /home/{{ ansible_user }}/.bashrc
          state: present
          line: source {{ ansible_user_dir }}/root_{{ root_version }}/bin/thisroot.sh
          create: yes
      
      - name: Add ROOT setup to .zshrc
        ansible.builtin.lineinfile:
          path: /home/{{ ansible_user }}/.zshrc
          state: present
          line: source {{ ansible_user_dir }}/root_{{ root_version }}/bin/thisroot.sh
          create: yes
    when: root_installation.stat.exists == False
