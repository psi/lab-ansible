#!/bin/bash

NProcessors=$(getconf _NPROCESSORS_ONLN)
NCores=$(( NProcessors > 1 ? (NProcessors - 1) : 1 ))

pwd
source setup.sh
mkdir -p build
cd build
cmake ..
make -j${NCores}
