#!/bin/sh
# Fail on any error
set -e
LIMIT_HOSTS=""

if [ $# -eq 1 ]; then
    LIMIT_HOSTS="--limit $1"
    echo "Limiting execution to $1"
fi

ansible-playbook ping.yml $LIMIT_HOSTS
echo "Enter user sudo password:"
read -s PASS
ansible-playbook -e "ansible_sudo_pass=$PASS" remove-sudo-pass.yml $LIMIT_HOSTS
ansible-playbook base.yml $LIMIT_HOSTS
ansible-playbook identify-nics.yml $LIMIT_HOSTS
echo "Please add the ethernet_card name to the inventory file if not already done so. Then hit enter."
read -s
ansible-playbook network.yml $LIMIT_HOSTS
ansible-playbook wireshark.yml $LIMIT_HOSTS
ansible-playbook protobuf.yml $LIMIT_HOSTS
ansible-playbook root.yml $LIMIT_HOSTS
# Optional
ansible-playbook direnv.yml $LIMIT_HOSTS
# Recommended
ansible-playbook ph2_acf.yml $LIMIT_HOSTS
ansible-playbook icicle.yml $LIMIT_HOSTS

# Optional
ansible-playbook docker.yml $LIMIT_HOSTS
ansible-playbook vscode.yml $LIMIT_HOSTS
ansible-playbook zsh.yml $LIMIT_HOSTS
