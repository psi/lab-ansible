# Ansible playbooks for CMS Phase-2 pixel setup

These playbooks automate the setup of PCs used for the CMS Phase-2 Tracker
upgrade.

## Initial setup

In order to facilitate running the scripts, you need to use authentication via SSH key.

This could e.g. be done as follows:

```shell
ssh-copy-id user@host
```

Add the machines that you would like to set up to the
[`hosts.yml` inventory file](./hosts.yml).
Remove all others. Initially, all hosts need the following information:

```yaml
pc11239:
    ansible_host: pc11239.psi.ch
    ansible_user: psihep
```

The `ansible_host` is the hostname, `ansible_user` is the username set up on
the machine.
The user needs to be set up with `sudo` privileges.
If this is not the case, log on to the machine as `root`
and add the user (replace `cmspixel` accordingly if using different username)
to the `wheel` (i.e. sudo) group:

```shell
sudo usermod -aG wheel cmspixel
```

Reboot the PC for the changes to take effect.

## Requirements

```shell
ansible-galaxy install -r requirements.yml
```

## Recommended execution order

First, make sure machines are reachable.
This command tries to connect to the machines and prints the Linux
distribution and version. Should this fail, make sure that you can `ssh` into
the machines manually.

```shell
ansible-playbook ping.yml
```

The following command will set up the base software on the machines.
As a first step, it will remove the need to issue a password when the `sudo`
command is used.
However, this means that the user account's password is needed once.
In order to avoid printing it on the command line and leaking it in your
shell history, execute the following command and then enter the user password,
followed by hitting enter:

```shell
read -s PASS
```

This will store the password in the variable `$PASS`.

For each machine (here `pc15827`), adjust machine name accordingly and also
update the `PASS` password if needed using again the `read` command above,
then execute the following command:

```shell
ansible-playbook -l pc15827 -e "ansible_sudo_pass=$PASS" remove-sudo-pass.yml
```

The next step installs some base software on the machines and also makes sure
that the user can access USB devicess without `sudo`:

```shell
ansible-playbook base.yml
```

Mind that the machine might reboot.

As a next step, we will identify the network interface controllers (NICs) on
the machines:

```shell
ansible-playbook identify-nics.yml
```

With a second ethernet card installed, this should print out two NICs.
Identify the one that has no IP or a self-assigned IP and/or connection status
"disconnected" or "connected. Add the interface name (e.g. `enp5s0` or
`enp1s0`) as variable `ethernet_card` to the
[`hosts.yml` inventory file](./hosts.yml) for the machine at the same level
as the `ansible_user`. Example:

```yaml
pc15827:
    ansible_host: pc15827.psi.ch
    ansible_user: cmspixel
    ethernet_card: enp1s0
```

We will now set a fixed IP address for this interface so that it can later
communicate with the FC7. The address is defined as `net_ip`
in [`vars.yml`](./vars.yml) (default: `192.168.1.1`).

```shell
ansible-playbook network.yml
```

Now connect the ethernet cable into the FC7s and switch them on so that we can
communicate with them. We will now identify their MAC addresses using wireshark
and add them to the `/etc/ethers` file as well as create entries in
`/etc/hosts`.
Furthermore, the `rarpd` will be installed such that the FC7s will receive a
fixed IP automatically. This IPs are defined through `fc7_ip` in
[`vars.yml`](./vars.yml) (default: `192.168.1.80`) and will be incremented
automatically for additional FC7s.

```shell
ansible-playbook wireshark.yml
```

FC7 MAC addresses are in the form of `08:00:30:00:XX:XX`.
If you know the MAC address of the FC7s already, you can add them to the
[`hosts.yml` inventory file](./hosts.yml) as `fc7_macs`. Example:

```yaml
pc15827:
    ansible_host: pc15827.psi.ch
    ansible_user: cmspixel
    ethernet_card: enp1s0
    fc7_macs:
      - 08:00:30:00:29:b0  # Use lower case letters!
```

You can obtain the MAC address using the FC7 serial number
[here](https://cms-tracker-daq.web.cern.ch/cms-tracker-daq/components/300_fc7_macs/).

Install protobuf (required by `Ph2_ACF`):

```shell
ansible-playbook protobuf.yml
```

Install ROOT, version needs to be set in [`vars.yml`](./vars.yml)
as `root_version` (default is set):

```shell
ansible-playbook root.yml
```

Optionally, the [`direnv`](https://direnv.net/) tool can be installed.
It helps automatically set up environments such as Python virtual environments
and additional environment variables:

```shell
ansible-playbook direnv.yml
```

Install [`Ph2_ACF`](https://gitlab.cern.ch/cms_tk_ph2/Ph2_ACF),
tag can be set in [`vars.yml`](./vars.yml) as `ph2_acf_tag`,
if none is provided, the last tage will be picked automatically:

```shell
ansible-playbook ph2_acf.yml
```

The `Ph2_ACF` software will be installed into the home directory
with the following structure:

```text
${HOME}
├── Ph2_ACF
│   ├── Ph2_ACF_HEAD  # Git repository HEAD
│   └── v4-15         # tag with compiled build directory
├── Ph2_ACF_runs
│   └── v4-15         # Run directory with direnv .envrc
│                     # Use this directory to run calibrations
```

Install Python tools including ICICLE into Ph2_ACF_runs directory:

```shell
ansible-playbook python_tools.yml
```

## Additional (optional) tools

[`docker`](https://docker.com) can be used to run software such as
[`Ph2_ACF`](https://gitlab.cern.ch/cms_tk_ph2/Ph2_ACF) in a container:

```shell
ansible-playbook docker.yml
```

[`Visual Studio Code(https://code.visualstudio.com/) is a popular graphical
text editor:

```shell
ansible-playbook vscode.yml
```

Change to `zsh` as default shell and install useful plugins:

```shell
ansible-playbook zsh.yml
```

Automatic kerberos token retrieval:

```shell
ansible-playbook k5start.yml
```

**Mind that this requires the existence of a `.keytab` file in the user's home.**
See [KB0003405](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003405)
for more information.

KDE instead of Gnome:

```shell
ansible-playbook kde.yml
```

NoMachine for remote control:

```shell
ansible-playbook nomachine.yml
```
